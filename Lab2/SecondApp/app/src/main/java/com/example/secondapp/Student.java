package com.example.secondapp;

public class Student {

    public String name;
    public String surname;

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String toString() {
        return this.name + " " + this.surname;
    }
}
