package com.example.secondapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Jan", "Szewczyk"));
        students.add(new Student("Andrzej", "Ustupski"));
        students.add(new Student("Michał", "Wall"));
        students.add(new Student("Michał", "Szymański"));
        students.add(new Student("Bartłomiej", "Wnuk"));

        ArrayAdapter<Student> studentsAdapter = new ArrayAdapter<Student>(this, android.R.layout.simple_list_item_1, students);

        ListView listView = (ListView) findViewById(R.id.list_item);
        listView.setAdapter(studentsAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String student = adapterView.getItemAtPosition(i).toString();
                Toast toast = Toast.makeText(getApplicationContext(), student, Toast.LENGTH_SHORT);
                toast.show();
                return true;
            }
        });
    }
}
